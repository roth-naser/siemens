<?php

function template_resources() {

	wp_enqueue_style('style', get_stylesheet_uri());

	//wp_enqueue_script( 'script', get_template_directory_uri() . '/assets/js/functions-min.js', array ( 'jquery' ), 1.0, true);

}

add_action('wp_enqueue_scripts', 'template_resources');

add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
	remove_action('wp_head', '_admin_bar_bump_cb');
}

register_nav_menus(array(
	'primary' => __( 'Primary Menu' ),
	'research' => __( 'Research Menu' )
));


add_image_size( 'category-thumb', 300, 9999 ); // 300 pixels wide (and unlimited height)

?>
