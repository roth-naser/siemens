<?php
/*
Template Name: Publications
*/


get_header();


?>

  <?php if ( !is_front_page() ): ?>


		<content>

		  <div class="content-wrap">
        <h1>Publications</h1>
				<?php

if( have_rows('publications') ):


    while ( have_rows('publications') ) : the_row();
?>
        <div class="publication">
          <a href="<?php the_sub_field('link'); ?>" target="_blank">
            <div class="pub__authors">
              <?php the_sub_field('authors'); ?>
            </div>
						<div class="pub__title">
              <?php the_sub_field('title'); ?>
            </div>
            <div class="pub__journal">
              <?php the_sub_field('journal'); ?>
            </div>
          </a>

        </div>

      	<?php


    endwhile;

else :

    // no rows found
endif;
?>

		  </div>

		</content>
<?php endif; ?>

<?php
get_footer();

?>
