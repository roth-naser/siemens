
jQuery( document ).ready(function() {

  jQuery(".toggle_button").click(function(){
    jQuery("nav").toggleClass("mobile-open");
    jQuery(".toggle_button").toggleClass("active");
  });

  jQuery(".toggle-background ul li:nth-child(1)").hover(function(){
    jQuery(".header--change").toggleClass("header--link-hover_1");

  });
  jQuery(".toggle-background ul li:nth-child(2)").hover(function(){
    jQuery(".header--change").toggleClass("header--link-hover_2");
  });
  jQuery(".toggle-background ul li:nth-child(3)").hover(function(){
    jQuery(".header--change").toggleClass("header--link-hover_3");
  });
  jQuery(".toggle-background ul li:nth-child(4)").hover(function(){
    jQuery(".header--change").toggleClass("header--link-hover_4");
  });
  jQuery(".toggle-background ul li:nth-child(5)").hover(function(){
    jQuery(".header--change").toggleClass("header--link-hover_5");
  });
  jQuery(".toggle-background ul li:nth-child(6)").hover(function(){
    jQuery(".header--change").toggleClass("header--link-hover_6");
  });


var navTop = jQuery(".secondary-nav").offset().top;

var stickyNav = function () {
    var scrollTop = jQuery(window).scrollTop();
        if (scrollTop > navTop) {
            jQuery(".secondary-nav").addClass('nav--fixed');

        } else {
            jQuery(".secondary-nav").removeClass('nav--fixed');
        }
};

stickyNav();
jQuery(window).scroll(function () {
    stickyNav();
});


});
