<!DOCTYPE html>
<html <?php language_attributes(); ?>style="margin-top: 0px">
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
	<title><?php bloginfo('name'); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
  <link rel="shortcut icon" href="assets/img/favicons/favicon.ico">
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700,800" rel="stylesheet">
  <script src="https://use.fontawesome.com/df5a644246.js"></script>
  <?php
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Datum in der Vergangenheit
?>
</head>

<body>


  <header>
      <headerwrap>
      <hleft>
 <a class="homelink" href="<?php echo home_url(); ?>">Siemens Lab</a>
    <div class="slogan">
      Temperature Detection and Thermoregulation
    </div>
          </hleft>

         <hright>
             <img src="/wp-content/themes/siemens/assets/img/trace.png" class="trace">
          </hright>

      </headerwrap>

  </header>
  <nav class="toggle-background">
    <div class="toggle_mobile">
      <div class="toggle_button">
        <i class="fa fa-bars" aria-hidden="true"></i> Menu
      </div>

    </div>
    <?php
      $args = array(
        'theme_location' => 'primary'
      );
    ?>

    <?php wp_nav_menu( $args ); ?>
  </nav>

  <?php if ( is_front_page() ): ?>
<div class="hero header--change">
</div>
	<?php else: ?>
<div class="header-img header--change">
</div>
<?php endif; ?>
<?php if ( !is_front_page() ): ?>
<?php
if ( has_nav_menu( 'research' ) ): ?>


<div class="secondary-nav-wrap">
  <div class="secondary-nav">
    <?php
      $args = array(
        'theme_location' => 'research'
      );
    ?>

    <?php wp_nav_menu( $args ); ?>
  </div>
</div>
<?php endif; ?>
<?php endif; ?>
