<?php
/*
Template Name: Ohne Initial
*/
get_header();


?>

  <?php if ( !is_front_page() ): ?>


		<content>

		  <div class="content-wrap">
				<?php while (have_posts()) : the_post(); ?>
						<div class="home-text">
							<?php the_content(); ?>
						</div>

				<?php endwhile; ?>

		  </div>

		</content>
<?php endif; ?>

<?php
get_footer();

?>
