


  <?php if ( is_front_page() ): ?>
    <div class="home-footer">
      <ul>
        <li>
          <a href="http://www.medizinische-fakultaet-hd.uni-heidelberg.de/Pharmakologisches-Institut.102627.0.html" target="_blank">University of Heidelberg Institute of Pharmacology</a>
        </li>
        <li>
          <a href="https://www.heidelberg-university-hospital.com/home/" target="_blank">Heidelberg University Hospital</a>
        </li>
      </ul>
    </div>
  <?php else: ?>
    <footer>
      <ul>
        <li>
          <a href="http://www.medizinische-fakultaet-hd.uni-heidelberg.de/Pharmakologisches-Institut.102627.0.html">University of Heidelberg Institute of Pharmacology</a>
        </li>
        <li>
          <a href="https://www.klinikum.uni-heidelberg.de/">Heidelberg University Hospital</a>
        </li>
        <li><a href="#">Last update: <?php site_last_modified(); ?></a> </li>
      </ul>


    </footer>
<?php endif; ?>


<script src="<?php echo get_site_url(); ?>/wp-content/themes/<?php echo get_template() ?>/assets/js/jquery-2.1.4.min.js"></script>
<script src="<?php echo get_site_url(); ?>/wp-content/themes/<?php echo get_template() ?>/assets/js/functions-min.js" type="text/javascript"></script>
</body>
</html>
