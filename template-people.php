<?php
/*
Template Name: People
*/


get_header();


?>

  <?php if ( !is_front_page() ): ?>


		<content>

		  <div class="content-wrap">
        <h1 class="ta-center">People</h1>

        <div class="group"><img src="<?php the_field('group'); ?>" style="width: 100%" /></div>

<div class="people_PI">

  <?php

if( have_rows('principal') ):


while ( have_rows('principal') ) : the_row();
?>
<div class="people_grid">
  <div class="principal-wrap">
    <div class="principal_left">

      <div class="people__image" style="background: url('<?php the_sub_field('image'); ?>'); background-size: cover; background-position: center;">
      </div>
      <div class="people__name">
        <a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('name'); ?></a>
      </div>
      <div class="people__title">
        <?php the_sub_field('title'); ?>
      </div>
  </div>
  <div class="principal_right">

<div class="CV">
<div class="CVdate">
<?php the_sub_field('cv1_date'); ?>
</div>
<div class="CV_text">
<a href="<?php the_sub_field('PhD_link'); ?>" target="_blank"><?php the_sub_field('PhD'); ?></a>
</div>
</div>
<div class="CV">
<div class="CVdate">
<?php the_sub_field('cv2_date'); ?>
</div>
<div class="CV_text">
<a href="<?php the_sub_field('CV2_link'); ?>" target="_blank" ><?php the_sub_field('CV2'); ?></a></div>
</div>

<div class="CV">
<div class="CVdate">

<?php the_sub_field('cv3_date'); ?>
</div>

<div class="CV_text">
<a href="<?php the_sub_field('CV3_link'); ?>" target="_blank"><?php the_sub_field('CV3'); ?></a>
</div>

</div>

<div class="CV">
<div class="CVdate">
<?php the_sub_field('cv4_date'); ?>
</div>
<div class="CV_text">
<a href="<?php the_sub_field('CV4_link'); ?>" target="_blank"><?php the_sub_field('CV4'); ?></a>
</div>
</div>

</div>
  </div>

  <?php


endwhile;

else :

// no rows found
endif;
?>

</div>

</div>





        <div class="people_grid">










        <?php

if( have_rows('people') ):


    while ( have_rows('people') ) : the_row();
?>
        <div class="people">

            <div class="people__image" style="background: url('<?php the_sub_field('image'); ?>'); background-size: cover; background-position: center;">
            </div>
						<div class="people__name">
              <a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('name'); ?></a>
            </div>
            <div class="people__title">
              <?php the_sub_field('title'); ?>
            </div>
            <div class="people__email">

            </div>


        </div>

      	<?php


    endwhile;

else :

    // no rows found
endif;
?>

		  </div>
</div>
		</content>
<?php endif; ?>

<?php
get_footer();

?>
