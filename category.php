<?php

get_header();


?>


		<content>

		  <div class="content-wrap initial">

        <h1 class="subsection_title">News</h1>


				<?php while (have_posts()) : the_post(); ?>


						<div class="post">
            <div class="post-title"> <?php the_title(); ?> </div>
            <div class="post-content">
            <div class="post-shadow">
							<?php the_content(); ?></div>
</div>
<p></p>
						</div>

				<?php endwhile; ?>

		  </div>

		</content>


<?php
get_footer();

?>
